# -*- coding: utf-8 -*-
PATH = """path_to_target = """+path_to_target+""""
path_to_OPERATIONS = """+find_path("../","OPERATIONS",'d').replace("\"","/\"")[1:]+"""

## fichiers journaux, infos et traitements
PATH_TO_MAIL ="""+path_to_target+"""mail_dest.tmp"
file_log ="""+path_to_target+"""LOG/file_log"""+titre+""""
PRAAT_time_log = "../PRAAT/time_log"
CUT_FILE = """+path_to_target+"""cut_file"
XSL = """+find_path("../","trs2xml_Semoral_V2.xsl")+"""

## dossiers
path_to_inXML = """+path_to_target+"""IN_XML/"
path_to_XML = """+path_to_target+"""XML/"

path_to_inWAV = """+path_to_target+"""IN_WAV/"
path_to_BIG_PART_WAV = """+path_to_target+"""BIG_PART/"
path_to_PART_WAV = """+path_to_target+"""PART_WAV/"

path_to_TXT = """+path_to_target+"""TXT/"

path_to_DIR_JTRANS = """+find_path("../","jtrans-master",'d').replace("\"","/\"")[1:]+"""

path_to_LOG = """+path_to_target+"""LOG/"
path_to_CSV = """+path_to_target+"""CSV/"
path_to_LOG_CSV = """+path_to_target+"""CSV/LOG/"
path_to_LOG_CSV_CUT_1 = """+path_to_target+"""CSV/LOG/CUT_1/"
path_to_LOG_CSV_CUT_2 = """+path_to_target+"""CSV/LOG/CUT_2/"
path_to_CSV_SLAM = """+path_to_target+"""CSV/SLAM/"
path_to_ANCHOR_CSV = """+path_to_target+"""CSV/ANCHOR/"
path_to_OUT_CSV = """+path_to_target+"""CSV/SLAM/OUT/"

path_to_IN_SLAM = """+path_to_target+"""SLAM_data/"
path_to_TMP_IN_SLAM = """+path_to_target+"""IN_SLAM_TMP/"
path_to_OUT_SLAM = """+path_to_target+"""SLAM_output/"

## scripts
BIENVENUE = """+find_path("../","bienvenue.py")+"""
INSTALL = """+find_path("../","install_component.py")+"""
VERIF_INSTALL = """+find_path("../","verif_or_install")+"""
NETTOYAGE = """+find_path("../","nettoyage.py")+"""
RENOMMAGE = """+find_path("../","renommage.py")+"""
SAX = """+find_path("../","traitement_saxon.py")+"""
XML = """+find_path("../","traitement_XML.py")+"""
PRE_DECOUPE = """+find_path("../","grosse_decoupe.py")+"""
DECOUPE = """+find_path("../","decoupe.py")+"""
DEPLACEMENT = """+find_path("../","deplacement.py")+"""
JTRANS = """+find_path("../","traitement_JTrans.py")+"""
SLAM_deplacement = """+find_path("../","SLAM_deplacement.py")+"""
T_SLAM = """+find_path("../","traitement_slam.py")+"""
SLAM = """+find_path("../","SLAM.py")+"""
TG2CSV = """+find_path("../","TG2CSV.py")+"""
ANCHOR = """+find_path("../","traitement_annotations.py")+"""
SAUVEGARDE = """+find_path("../","fin_sauvegarde.py")+"""

## completion de commande
cmd_muet = " >/dev/null 2>&1"
cmd_silence = " >/dev/null 2>>"""+path_to_target.replace("\"","")+"""LOG/file_log"""+titre+""""

## programmes
PYTHON = "python2.7"
"""
