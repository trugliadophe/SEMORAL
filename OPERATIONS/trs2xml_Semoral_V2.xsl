<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exslt="http://exslt.org/common"
    version="2.0">
    
    <xsl:output method="xml" indent="yes" encoding="utf-8"/>
    
    <!-- **************************************
	l'idée est de garder l'information du chevauchement
	et de garder les balises de l'annotation glozz
	*************************************** -->
	<xsl:template match="/">
		<SEMORAL>
			<xsl:apply-templates/>
		</SEMORAL>
	</xsl:template>
	
	<!-- Les attributs -->
    <xsl:template match="@*">
        <xsl:copy-of select="."/>
    </xsl:template>
	
    <!-- Les elements de la dtd de Transcriber -->
	
    <xsl:template match="Trans">
		<TRANS>
			<LOCS>
				<xsl:for-each select="//Speakers">
					<xsl:apply-templates select="Speaker"/>
				</xsl:for-each>
			</LOCS>
			<TEXT>
				<xsl:copy-of select="@id"/>
				<xsl:apply-templates select=".//Turn"/>
			</TEXT>
		</TRANS>
    </xsl:template>
	
	<xsl:template match="annotations">
		<xsl:copy-of select="."/>
    </xsl:template>
	
	<xsl:template match ="Speaker">
		<LOC>
			<xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
		</LOC>
	</xsl:template>
	
    <xsl:template match="Turn">
        <xsl:for-each select="Sync">
            <xsl:variable name="sync" select="generate-id(.)"/>
            <xsl:variable name="startTime" select="@time"/>
            <xsl:variable name="pos" select="position()"/>
            <xsl:variable name="endTime">
                <xsl:choose>
                    <xsl:when test="ancestor::Turn/Sync[number($pos + 1)]">
                        <xsl:value-of select="ancestor::Turn/Sync[number($pos + 1)]/@time"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="ancestor::Turn/@endTime"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="count(tokenize(ancestor::Turn/@speaker, ' ')) &gt; 1">
                    <xsl:for-each select="following-sibling::Who">
                        <xsl:variable name="num" select="@nb"/>
                        <xsl:if test="generate-id(preceding-sibling::Sync[1]) = $sync">
                           <S id="s{generate-id(.)}">
								<xsl:attribute name="overlap">oui</xsl:attribute>
                                <xsl:attribute name="who">
                                    <xsl:variable name="pseudo" select="tokenize(ancestor::Turn/@speaker, ' ')[number($num)]"/>
									<xsl:variable name="pseudoName" select="//Speakers/Speaker[@id=$pseudo]/@name"/>
									<xsl:value-of select="$pseudoName"/>
                                </xsl:attribute>
                                <AUDIO start="{$startTime}" end="{$endTime}"/>
                                <FORM>
                                    <xsl:for-each select="following-sibling::text()[generate-id(preceding-sibling::Sync[1]) = $sync][preceding-sibling::Who[1]/@nb=$num]|following-sibling::Event[generate-id(preceding-sibling::Sync[1]) = $sync][preceding-sibling::Who[1]/@nb=$num]|following-sibling::Comment[generate-id(preceding-sibling::Sync[1]) = $sync][preceding-sibling::Who[1]/@nb=$num]|following-sibling::anchor[generate-id(preceding-sibling::Sync[1]) = $sync][preceding-sibling::Who[1]/@nb=$num]">
                                        <P><xsl:apply-templates select="."/></P>
                                    </xsl:for-each>
                                </FORM>
                            </S>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <S id="s{generate-id(.)}" overlap="non">
					<xsl:attribute name="who">
							<xsl:variable name="pseudo2" select="ancestor::Turn/@speaker"/>
							<xsl:variable name="pseudoName2" select="//Speakers/Speaker[@id=$pseudo2]/@name"/>
							<xsl:value-of select="$pseudoName2"/>
						</xsl:attribute>
						<AUDIO start="{$startTime}" end="{$endTime}"/>
                        <xsl:variable name="form">
                            <xsl:for-each select="following-sibling::text()[generate-id(preceding-sibling::Sync[1])=$sync]|following-sibling::anchor[generate-id(preceding-sibling::Sync[1])=$sync]">
                                <P><xsl:apply-templates select="."/></P>
                                <xsl:if test="position()!=last()">
                                    <xsl:text> </xsl:text>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:variable>
                        <FORM>
                            <xsl:for-each select="following-sibling::text()[generate-id(preceding-sibling::Sync[1])=$sync]|following-sibling::Event[generate-id(preceding-sibling::Sync[1])=$sync]|following-sibling::Comment[generate-id(preceding-sibling::Sync[1])=$sync]|following-sibling::anchor[generate-id(preceding-sibling::Sync[1])=$sync]">
                                <P><xsl:apply-templates select="."/></P>
                            </xsl:for-each>
                        </FORM>
                    </S>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="anchor">
	<anchor>
	<xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
	<xsl:attribute name="num"><xsl:value-of select="@num"/></xsl:attribute>
		<xsl:for-each select="text()|anchor">
            <xsl:apply-templates select="."/>
        </xsl:for-each>
	</anchor></xsl:template>
    <xsl:template match="Comment"></xsl:template><!--on supprime les informations sur les évènements-->
    <xsl:template match="Event"></xsl:template><!-- on supprime les informations sur les commentaires-->
</xsl:stylesheet>