# -*- coding: utf-8 -*-
import pygame.locals
import os, sys
from datetime import datetime
with open("./PATH.py",'r') as f_path:
	path = f_path.read()
	f_path.close()
	exec(path)
fin = False
dico_option = {0:(path_to_inXML,"xml (base)"),
1:(path_to_XML,"xml (apres passage XSL)"),
2:(path_to_LOG_CSV_CUT_1,"csv (big part)"),
3:(path_to_LOG_CSV_CUT_2,"csv (part)"),
4:(path_to_ANCHOR_CSV,"csv (annotation anchor)"),
5:(path_to_OUT_CSV,"csv (sortie slam)"),
6:(path_to_inWAV,"wav (base)"),
7:(path_to_BIG_PART_WAV,"wav (big part)"),
8:(path_to_PART_WAV,"wav (part)"),
9:(path_to_TXT,"txt (une phrase par ligne)"),
10:(path_to_IN_SLAM,"TextGrid (alignement)"),
11:(path_to_OUT_SLAM,"TextGrid (alignement + slam)"),
12:(file_log,"fichier journal")}
option = [v[1] for k,v in dico_option.items()]
option_yes = [False for i in range(len(option))]
choix = ""
while not choix in ['q','Q']:
	os.system('clear')
	sys.stdout.write("Option(s) possible(s) :\n")
	if False not in option_yes:
		sys.stdout.write("\tAucune option")
	for e in [str(i)+":"+option[i] for i in range(len(option)) if not option_yes[i]]:
		sys.stdout.write("\t"+e+"\n")
	sys.stdout.write("\n\nOption(s) choisie(s) :\n")
	if True not in option_yes:
		sys.stdout.write("\tAucune option")
	for e in [str(i)+":"+option[i] for i in range(len(option)) if option_yes[i]]:
		sys.stdout.write("\t"+e+"\n")
	choix = raw_input("\n\n\nEntrez le numero de l'option que vous voulez (de)selectionner\nPour inverser la selection entrer 'inv'\nSi vous avez fini votre choix, entrez 'q'\npuis appuyez sur entrer : \n")
	if choix == 'inv':
		for i in range(len(option_yes)):
			option_yes[i] = not option_yes[i]
	elif choix not in [str(i) for i in range(len(option))]+['q','Q']:
		a = raw_input("Erreur de saisie, appuyer sur entrer pour continuer\n")
	elif choix in ['q','Q']:
		a = raw_input("Construction de la commande, appuyer sur entrer pour commencer !\n")
	elif int(choix) in range(len(option)):
		option_yes[int(choix)] = not option_yes[int(choix)]

name = "result_"+file_log.split("file_log")[1]+".tar "
commande = "tar cvf "+path_to_target+name

for k,v in dico_option.items():
	commande += " "+os.path.relpath(v[0],path_to_target) if option_yes[k] else ""
os.system('cd '+path_to_target+';'+commande+';cd '+path_to_OPERATIONS)

os.system('tar -tf '+path_to_target+name+' | wc -l > nb_fichiers')
with open('nb_fichiers','r') as nb_fichiers:
	nb = int(nb_fichiers.readline())
sys.stdout.write("retrouvez ces "+str(nb)+" fichiers archivés dans "+path_to_target+name+"\n")
