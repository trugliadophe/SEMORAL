# -*- coding: utf-8 -*-
import glob
import os, sys
with open("./PATH.py",'r') as f_path:
	path = f_path.read()
	exec(path)
	f_path.close()

list_glob = glob.glob(path_to_inXML+"*.xml")
list_glob.sort()
sys.stdout.write("Renommage des fichiers pour une meilleure lecture")
for element in list_glob:
	old = os.path.split(element)[1]
	element = os.path.split(element)[1]
	element = element.replace("corpus_","")
	element = element.replace("\\","_")
	element = element.replace("annotation_integree_","")
	element = element.replace("-","_")
	element = element.replace("__","_")
	element = element.replace("C_","")
	element = element.replace("C-","")
	element = element.replace("_C.xml",".xml")
	element = element.lower()
	if not old == element:
		os.system('mv '+path_to_inXML+old+' '+path_to_inXML+element+" "+cmd_silence)
sys.stdout.write(" ... ")
list_glob = glob.glob(path_to_inWAV+"*")
list_glob.sort()

for element in list_glob:
	old = os.path.split(element)[1]
	element = os.path.split(element)[1]
	element = element.replace("ESLO1_","ESLO_")
	element = element.replace("ENT_","")
	element = element.replace("ENTCONT_","")
	element = element.replace("REU_","")
	element = element.replace("C_","")
	element = element.replace("C-","")
	element = element.replace(".WAV",".wav")
	element = element.lower()
	if not old == element:
		os.system('mv '+path_to_inWAV+old+' '+path_to_inWAV+element+" "+cmd_silence)

sys.stdout.write("Fait !\n")
