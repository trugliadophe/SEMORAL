# -*- coding: utf-8 -*-
import os, sys, re, csv, glob
from lxml import etree

with open("./PATH.py",'r') as f_path:
	path = f_path.read()
	exec(path)
	f_path.close()

def textgrid2xml(element):
	with open(path_to_OUT_SLAM+element, "r") as txtgrid:
		with open(path_to_OUT_SLAM+element+"_tmp.xml","w") as outfile:
			outfile.write('<?xml version="1.0" encoding="UTF-8"?>\n<tiers>')
			for line in txtgrid:
				line = re.sub(r'^xmax = (.*)', r'<time>\1</time>', line)
				line = re.sub(r'\titem \[(.*)\]:', r'</item><item id="\1">', line)
				line = re.sub(r'name = "(.*)"', r'<name>\1</name>', line)				
				line = re.sub(r'intervals \[(.*)\]:', r'<intervals id="\1">', line)
				line = re.sub(r'text = "(.*)"', r'\1</intervals>\n', line)
		
				if ("<time>" in line) or ("<item" in line) or ("<name>" in line) or ("<intervals" in line) or ("intervals>" in line) :
					if '<item id="1">' in line: 
						outfile.write('<item id="1">')
					else:
						outfile.write(line)
			
			outfile.write("</item></tiers>")
			outfile.close()
		txtgrid.close()

def xml2xml_sansmotvide(element):
	list_id = []
	list_lines_out = []
	with open(path_to_OUT_SLAM+element+"_tmp.xml",'r') as inXML:
		list_lines = inXML.readlines()
		i=0
		while i < len(list_lines):
			if "<intervals id=\"" in list_lines[i]:#si c'est le debut d'un interval
				tmp_id = int(list_lines[i].split("<intervals id=\"")[1].split("\">")[0])#on stock l'id de l'interval
				if not tmp_id in list_id:#si l'id n'est pas connu
					if not "</intervals>" == list_lines[i+1].replace("\n","").replace("\t"," ").replace("  "," ").replace(" ",""):#si la ligne suivante contient un mot, on rajoute la ligne a la liste de ligne a ecrire
						list_lines_out.append(list_lines[i])
					else:
						list_id.append(tmp_id)
						i+=1
				else:
					i+=1
			else:
				list_lines_out.append(list_lines[i])
			i+=1
		inXML.close()
	os.system('rm '+path_to_OUT_SLAM+element+"_tmp.xml")
	with open(path_to_OUT_SLAM+element+"_tmp.xml","w") as outXML:
		for line in list_lines_out:
			if not line.replace("\n","").replace(" ","") == "":
				outXML.write(line)
		outXML.close()

def textGrid2csv(element):
	textgrid2xml(element)
	xml2xml_sansmotvide(element)
	dict = {}
	with open(path_to_LOG_CSV_CUT_2+element.split(".")[0].split("_part_")[0]+"_log.csv", "r") as fileStartEnd:
		filecsv = csv.reader(fileStartEnd, delimiter=';')
		for line in filecsv:
			if len(line) > 0:
				dict[line[0]+"_start"] = line[1]
				dict[line[0]+"_end"] = line[2]
				dict[line[0]+"_txt"] = line[3]
		fileStartEnd.close()
	fname = element.split(".")[0]
	
	with open(path_to_OUT_SLAM+element+"_tmp.xml", "r") as xml:
		tree = etree.parse(xml)
		xml.close()
	
	start = dict[fname+"_start"]
	end = dict[fname+"_end"]
	txt = dict[fname+"_txt"]
	with open(path_to_CSV_SLAM+fname+".csv","w") as outfilecsv:
		outfilecsv.write(fname+";"+tree.findtext("time").replace("\n", "")+";"+start+";"+end+";"+txt)
		for item in tree.xpath('//item'):
			if "Style" in item.findtext('name'):
				for interval in item.findall('intervals'):
					#if not("_" in interval.text):
					outfilecsv.write(";"+interval.text.replace("\n", "").replace("\t", ""))
				outfilecsv.write("\n")
		outfilecsv.close()

sys.stdout.write("Transformation TextGrid > CSV :\n")
sys.stdout.flush()
list_element = os.listdir(path_to_OUT_SLAM)
i=0
chaine = ""
for element in list_element:
	i+=1
	sys.stdout.write("\r"+" "*(len(chaine)+20))
	sys.stdout.flush()
	chaine = "\ttraitement "+element+"\t"+str(i*100/len(list_element))+"%"
	sys.stdout.write("\r"+chaine)
	sys.stdout.flush()
	textGrid2csv(element)
#	os.system('rm '+path_to_OUT_SLAM+element+"_tmp.xml")
sys.stdout.write("\n")
sys.stdout.flush()
