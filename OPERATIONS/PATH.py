path_to_target = "/home/leroux/VM_dir/partage/stage_16/"
path_to_OPERATIONS = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/"

## fichiers journaux, infos et traitements
PATH_TO_MAIL ="/home/leroux/VM_dir/partage/stage_16/mail_dest.tmp"
file_log ="/home/leroux/VM_dir/partage/stage_16/LOG/file_log_13_07_2016__01_42_35"
PRAAT_time_log = "../PRAAT/time_log"
CUT_FILE = "/home/leroux/VM_dir/partage/stage_16/cut_file"
XSL = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/trs2xml_Semoral_V2.xsl"

## dossiers
path_to_inXML = "/home/leroux/VM_dir/partage/stage_16/IN_XML/"
path_to_XML = "/home/leroux/VM_dir/partage/stage_16/XML/"

path_to_inWAV = "/home/leroux/VM_dir/partage/stage_16/IN_WAV/"
path_to_BIG_PART_WAV = "/home/leroux/VM_dir/partage/stage_16/BIG_PART/"
path_to_PART_WAV = "/home/leroux/VM_dir/partage/stage_16/PART_WAV/"

path_to_TXT = "/home/leroux/VM_dir/partage/stage_16/TXT/"

path_to_DIR_JTRANS = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/jtrans-master/"

path_to_LOG = "/home/leroux/VM_dir/partage/stage_16/LOG/"
path_to_CSV = "/home/leroux/VM_dir/partage/stage_16/CSV/"
path_to_LOG_CSV = "/home/leroux/VM_dir/partage/stage_16/CSV/LOG/"
path_to_LOG_CSV_CUT_1 = "/home/leroux/VM_dir/partage/stage_16/CSV/LOG/CUT_1/"
path_to_LOG_CSV_CUT_2 = "/home/leroux/VM_dir/partage/stage_16/CSV/LOG/CUT_2/"
path_to_CSV_SLAM = "/home/leroux/VM_dir/partage/stage_16/CSV/SLAM/"
path_to_ANCHOR_CSV = "/home/leroux/VM_dir/partage/stage_16/CSV/ANCHOR/"
path_to_OUT_CSV = "/home/leroux/VM_dir/partage/stage_16/CSV/SLAM/OUT/"

path_to_IN_SLAM = "/home/leroux/VM_dir/partage/stage_16/SLAM_data/"
path_to_TMP_IN_SLAM = "/home/leroux/VM_dir/partage/stage_16/IN_SLAM_TMP/"
path_to_OUT_SLAM = "/home/leroux/VM_dir/partage/stage_16/SLAM_output/"

## scripts
BIENVENUE = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/bienvenue.py"
INSTALL = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/install_component.py"
VERIF_INSTALL = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/verif_or_install"
NETTOYAGE = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/nettoyage.py"
RENOMMAGE = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/renommage.py"
SAX = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/traitement_saxon.py"
XML = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/traitement_XML.py"
PRE_DECOUPE = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/grosse_decoupe.py"
DECOUPE = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/decoupe.py"
DEPLACEMENT = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/deplacement.py"
JTRANS = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/traitement_JTrans.py"
SLAM_deplacement = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/SLAM_deplacement.py"
T_SLAM = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/traitement_slam.py"
SLAM = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/SLAM.py"
TG2CSV = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/TG2CSV.py"
ANCHOR = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/traitement_annotations.py"
SAUVEGARDE = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/fin_sauvegarde.py"

## completion de commande
cmd_muet = " >/dev/null 2>&1"
cmd_silence = " >/dev/null 2>>/home/leroux/VM_dir/partage/stage_16/LOG/file_log_13_07_2016__01_42_35"

## programmes
PYTHON = "python2.7"


SAXON = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/SaxonHE9-5-1-7J/saxon9he.jar"
JTrans = "/home/leroux/VM_dir/partage/stage_16/prog/OPERATIONS/jtrans-master/jtrans.jar"