# -*- coding: utf-8 -*-
from datetime import datetime
import os, sys

with open("./PATH.py",'r') as f_path:
	path = f_path.read()
	exec(path)
	f_path.close()

sys.stdout.write("nettoyage des anciennes donnees\n")
list_dirs = [path_to_XML, path_to_BIG_PART_WAV, path_to_PART_WAV, path_to_TXT, path_to_CSV, path_to_LOG_CSV,path_to_LOG_CSV_CUT_1,path_to_LOG_CSV_CUT_2, path_to_CSV_SLAM, path_to_OUT_CSV, path_to_ANCHOR_CSV, path_to_IN_SLAM, path_to_OUT_SLAM]

sys.stdout.write("Attention\nLes dossiers suivants vont etre nettoyes, toutes les donnees contenues seront perdues :\n")
os.system('notify-send "Attention les dossiers suivants vont etre nettoyes, toutes les donnees contenues seront perdues \nSuppression de dossier(s) et/ou fichier(s)\nVeillez a sauvegarder les donnees avant de continuer !"')
cpt = 0
for e in list_dirs:
	if os.path.exists(e):
		sys.stdout.write(e+"\n")
		sys.stdout.flush()
		cpt += len(os.listdir(e))+1
a = raw_input("Suppression de "+str(cpt)+" dossiers et fichiers\nVeillez a sauvegarder les donnees avant de continuer !\nPour continuer appuyez sur entrer ! ")
sys.stdout.flush()
today = datetime.now()
with open(file_log,'w') as fl:
	titre = " LOGFILE : """+str(today.day).zfill(2)+"/"+str(today.month).zfill(2)+"/"+str(today.year).zfill(4)+" "+str(today.hour).zfill(2)+":"+str(today.minute).zfill(2)+":"+str(today.second).zfill(2)+" " 
	fl.write("\n\n\n")
	fl.write("#"*100+"\n")
	fl.write(titre+"\n")
	fl.write("#"*100+"\n")
	fl.close()
print file_log+" exists ? : "+str(os.path.exists(file_log))
sys.stdout.write("Suppression des donnees ["+" "*10+"]")
i=0
for dirs in list_dirs:
	i+=1
	cpt = i*10/len(list_dirs)
	sys.stdout.write("\rSuppression des donnees ["+"|"*cpt+" "*(10-cpt)+"]")
	sys.stdout.flush()
	if os.path.exists(dirs):
		os.system("rm -rf "+dirs+cmd_silence)
	if not os.path.exists(dirs):
		os.system("mkdir "+dirs+cmd_silence)
sys.stdout.write(" Fait !\nCreation de l'espace journal : Retrouvez les erreurs non-critiques dans "+ file_log+"\n")
sys.stdout.flush()
