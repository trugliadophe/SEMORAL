# -*- coding: utf-8 -*-
import os
from lxml import etree as ET
import sys
with open("./PATH.py",'r') as f_path:
	path = f_path.read()
	exec(path)
	f_path.close()

list_element = os.listdir(path_to_XML)
list_element.sort()

with open(path_to_LOG_CSV_CUT_1+'big_part.csv','a') as csv:

	i=0
	sys.stdout.write("decoupe des fichiers WAV en parties correspondantes aux fichiers XML\n")
	for element in list_element:
		i+=1
		#"+str(i).zfill(3)+"/"+str(len(list_element)).zfill(3)+"
		sys.stdout.write("\r\tdecoupe des fichiers "+str(100*i/len(list_element))+" % XML --> CSV")
		sys.stdout.flush()
		tree =  ET.parse(path_to_XML+element)
		list_AUDIO = tree.xpath("/SEMORAL/TRANS/TEXT/S/AUDIO")

		time = float(list_AUDIO[0].get('start'))
		time_m = int(time/60.000)
		time_s = int(time%60)
		chaine1 = '{:01d}'.format(time_m)+':{:02d}'.format(time_s)+'.{:03d}'.format(1+int(1000*float(float(time%60.0)%1)))
		time = float(list_AUDIO[-1].get('end'))-0.001
		time_m = int(time/60.000)
		time_s = int(time%60)
		chaine2 = '{:01d}'.format(time_m)+':{:02d}'.format(time_s)+'.{:03d}'.format(int(1000*float(float(time%60.0)%1)))
		(name,ext) = element.split(".")
		if len(name.split("_")) <= 2:
			(corpus,num) = name.split("_")[:2]
			part = ""
		else:
			(corpus,num,part) = name.split("_")[:3]
		wav_name = corpus+"_"+num+".wav"

		csv.write(name.lower()+";"+wav_name.lower()+";"+chaine1+";"+chaine2+"\n")
	csv.close()
sys.stdout.write("... Fait !\n")
with open(path_to_LOG_CSV_CUT_1+'big_part.csv','r') as csv:
	for line in csv.readlines():
		sys.stdout.flush()
		if len(line.split(";")) == 4:
			(n,f,chaine1,chaine2) = line.split(";")
			n = n.lower()
			sys.stdout.write("\r\tdecoupe des fichiers CSV --> big_part_WAV ("+f+" --> "+n+")")
			with open(CUT_FILE,'w') as cut_file:
				cut_file.write(chaine1 + "\n" + chaine2)
				cut_file.close()
			commande = "shnsplit -O always -f "+CUT_FILE+" -o wav "+path_to_inWAV+f+" -x 2 -d "+path_to_BIG_PART_WAV+" -n '' -d "+path_to_BIG_PART_WAV+" -a "+n.lower()
			commande += " "+cmd_muet
			os.system(commande)
	csv.close()
sys.stdout.write("... Fait !\n")
