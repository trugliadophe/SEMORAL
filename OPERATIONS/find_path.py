# -*- coding: utf-8 -*-
import os, sys
from datetime import datetime
def find_path(directory,name,opt='f'):
	chaine = "recherche du "+("fichier " if opt == 'f' else "dossier ")+name+" dans "+directory
	sys.stdout.write("\r"+chaine)
	sys.stdout.flush()
	for path, dirs, files in os.walk(directory):
		liste = dirs if opt == 'd' else files
		for a in liste:
			if a == name:
				result = "\""+os.path.abspath(str(path)+"/"+a)+"\""
				chaine += ">>"+result
				sys.stdout.write("\r"+chaine+"\n")
				return result
def verification(path):
	verif = [path.replace("\"","")+"LOG/"]
	for v in verif:
		if not os.path.exists(v):
			os.system("mkdir "+v)
def init():
	path_to_target = find_path("/","TARGET_PROSODORUS",'f').replace("TARGET_PROSODORUS","").replace("/\"","/")
	if not path_to_target == None:
		verification(path_to_target)
		sys.stdout.write("\ninsertion des noms et chemins de fichiers necessaires aux traitements\n")
		sys.stdout.flush()
		today = datetime.now()
		titre = "_"+str(today.day).zfill(2)+"_"+str(today.month).zfill(2)+"_"+str(today.year).zfill(4)+"__"+str(today.hour).zfill(2)+"_"+str(today.minute).zfill(2)+"_"+str(today.second).zfill(2)
	
		with open('template_path.py','r') as IN:
			content = IN.read()
			exec(content)
			with open("./PATH.py",'w') as OUT:
				OUT.write(PATH)
				OUT.close()
			IN.close()
	else:
		try:
			os.system('rm ./PATH.py >/dev/null 2<&1')
		finally:
			sys.stdout.write("Fichier cible introuvable !\nVeuillez vous reportez aux instructions contenues dans "+find_path("../","README.md"))

def programmes():
	with open('template_programmes.py','r') as IN:
		content = IN.read()
		exec(content)
		with open("./PATH.py",'a') as OUT:
			OUT.write(PATH)
			OUT.close()
		IN.close()
