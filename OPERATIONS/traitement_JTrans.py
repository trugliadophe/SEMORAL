# -*- coding: utf-8 -*-
import os, sys
with open("./PATH.py",'r') as f_path:
	path = f_path.read()
	exec(path)
	f_path.close()

list_WAV = os.listdir(path_to_PART_WAV)
list_WAV.sort()
sys.stdout.write("Traitement JTrans\n")
sys.stdout.flush()
chaine = ""
chaine_fait = ""
OUTfmt = "textgridw"
i=0
for inWAV in list_WAV:
	i+=1
	if os.path.exists(path_to_TXT+inWAV.split(".")[0]+".txt"):
		inTXT = inWAV.split(".")[0]+".txt"
		name = inWAV.split("_")[0]+inWAV.split("_")[1]
		big_part = inWAV.split("_")[2]
		part = inWAV.split("_")[-1].split(".")[0]
		sys.stdout.write("\r"+" "*(len(chaine+chaine_fait)+20))
		chaine = "\ttraitement fichier : "+name+" BigPart : "+big_part+" Part : "+part+"\t"+str((i*100)/len(list_WAV))+"%"
		sys.stdout.write("\r"+chaine)
		sys.stdout.flush()
		os.system('java -jar '+JTrans+' -a '+path_to_PART_WAV+inWAV+' -f '+path_to_TXT+inTXT+' -outdir '+path_to_IN_SLAM+' -outfmt '+OUTfmt+cmd_muet)
		with open(path_to_IN_SLAM+inWAV.replace(".wav",".w.textgrid"),'r') as INtmp:
			with open(path_to_IN_SLAM+inWAV.replace(".wav",".TextGrid"),'w') as OUTtmp:
				OUTtmp.write(INtmp.read().replace("Unknown words","words"))
				OUTtmp.close()
			INtmp.close()
		os.system('rm '+path_to_IN_SLAM+inWAV.replace(".wav",".w_anon.textgrid"))
		os.system('rm '+path_to_IN_SLAM+inWAV.replace(".wav",".w.textgrid"))
		chaine_fait = "... Fait !"
		sys.stdout.write(chaine_fait)
		sys.stdout.flush()
		os.system('cp '+path_to_PART_WAV+inWAV+' '+path_to_IN_SLAM)
sys.stdout.write("\n")
