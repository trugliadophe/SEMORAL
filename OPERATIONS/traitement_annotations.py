# -*- coding: utf-8 -*-
import os, sys
with open("./PATH.py",'r') as f_path:
	path = f_path.read()
	exec(path)
sys.stdout.write("Traitement des annotations :\n")
dico_anchor = {}
#chaine = ""
#i=0
#list_element = os.listdir(path_to_ANCHOR_CSV)
#for element in list_element:
#	i+=1
#	sys.stdout.write("\r"+" "*(len(chaine)+20))
#	sys.stdout.flush()
#	chaine = "\ttraitement "+element+"\t"+str(i*100/len(list_element))+"%"
#	sys.stdout.write("\r"+chaine)
#	sys.stdout.flush()
#	with open(path_to_ANCHOR_CSV+element,'r') as inANCHOR:
#		for ANCHORline in inANCHOR.readlines():
#			ANCHORline = ANCHORline.replace("\n","").replace("  "," ")
#			if os.path.exists(path_to_CSV_SLAM+ANCHORline.split(";")[0].lower()+".csv"):
#				with open(path_to_CSV_SLAM+ANCHORline.split(";")[0].lower()+".csv",'r') as inCSV:
#					for CSVline in inCSV.readlines():
#						CSVline = CSVline.replace("\n","").replace("  "," ")
#						if ANCHORline.split(";")[1] in CSVline.split(";")[4]:
#							sys.stdout.write("\nANCHOR LINE : "+ANCHORline+"\nCSVline : "+CSVline)
#							dico_anchor[ANCHORline.split(";")[2]+"_"+ANCHORline.split(";")[3]] = {'txt':CSVline.split(";")[4].split(" "),'prosodie':CSVline.split(";")[5:]}
#					inCSV.close()
#		inANCHOR.close()
#sys.stdout.write("\nEcriture des données d'annotations dans le fichier final : "+path_to_OUT_CSV+"\n")
#print dico_anchor
#with open(path_to_OUT_CSV+"out_slam",'w') as outCSV:
#	outCSV.write("name;num;id;txt++;prosodie++;\n")
#	for k,v in dico_anchor.items():
#		outCSV.write(k.lower().split("_")[0]+";"+k.lower().split("_")[1]+";"+k.lower().split("_")[2]+";")
#		for t, p in v.items():
#			if t == "txt":
#				outCSV.write("TXT;")
#				for text in p:
#					outCSV.write(text+";")
#			elif t == "prosodie":
#				outCSV.write("PROSODIE;")
#				for prosodie in p:
#					outCSV.write(prosodie+";")
#				outCSV.write("\n")
#	outCSV.close()
##########################################
##########################################
chaine = ""
i=0
list_element = os.listdir(path_to_CSV_SLAM)
for element in list_element:
	with open(path_to_CSV_SLAM+element,'r') as inCSV:
		for CSVline in inCSV.readlines():
			with open(path_to_ANCHOR_CSV+"_".join(CSVline.split("_")[:3])+"_anchor.csv") as inANCHOR:
				ANCHORline = ""
				while not ANCHORline == None and not ANCHORline.split(";")[0] == CSVline.split(";")[0]:
					ANCHORline = inANCHOR.readline()
					if ANCHORline == None:
						sys.stdout.write("\n"+ANCHORline)
				sys.stdout.write("ANCHORline = "+ANCHORline+"\nCSVline = "+CSVline)
