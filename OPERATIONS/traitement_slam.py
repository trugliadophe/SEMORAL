# -*- coding: utf-8 -*-
import os, sys, glob
with open("./PATH.py",'r') as f_path:
	path = f_path.read()
	f_path.close()
	exec(path)
sys.stdout.write("Traitement Slam :\n")
list_element = glob.glob(path_to_IN_SLAM+'*.wav')
list_element.sort()
i=0
chaine = ""
for element in list_element:
	i+=1
	sys.stdout.write("\r"*(len(chaine)+20))
	chaine = "\ttraitement SLAM : "+element+"\t"+str((i*100)/len(list_element)).zfill(3)+"%"
	sys.stdout.write("\r"+chaine)
	sys.stdout.flush()
	if os.path.exists(path_to_TMP_IN_SLAM):	
		os.system('rm -rf '+path_to_TMP_IN_SLAM)
	os.system('mkdir '+path_to_TMP_IN_SLAM)
	if os.path.exists(element.replace(".wav",".TextGrid")):
		os.system('cp '+element+' '+path_to_TMP_IN_SLAM)
		os.system('cp '+element.replace(".wav",".TextGrid")+' '+path_to_TMP_IN_SLAM)
		with open(file_log,'a') as log:
			log.write("################ "+element+" ################\n")
			log.close()
		os.system(PYTHON+' '+SLAM+cmd_silence)
sys.stdout.write("\n")
