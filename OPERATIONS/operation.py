# -*- coding: utf-8 -*-
import os
import sys
import time
import smtplib
from mail import *
from find_path import init,programmes

start_time = time.time()
init()# from find_path
with open("./PATH.py",'r') as f_path:
	path = f_path.read()
	f_path.close()
	exec(path)
args = ""
for e in sys.argv:
	args+= e+" "
os.system(PYTHON+' '+BIENVENUE+' '+args)
programmes() # from find_path
with open("./PATH.py",'r') as f_path:
	path = f_path.read()
	f_path.close()
	exec(path)
time_to_init = time.time() - start_time

os.system('clear')

start_time = time.time()
os.system(PYTHON+' '+NETTOYAGE)
#os.system(PYTHON+' '+RENOMMAGE)
os.system(PYTHON+' '+DECOUPE)
time_to_start = time.time() - start_time
alert_mail("début traitement JTRANS\n")

start_time = time.time()
os.system(PYTHON+' '+JTRANS)
time_to_align = time.time() - start_time

alert_mail("début traitement SLAM\n")

start_time = time.time()
os.system(PYTHON+' '+T_SLAM)
sys.stdout.write("ok")
time_to_prosodie = time.time() - start_time

alert_mail("début traitement annotations\n")

start_time = time.time()
os.system(PYTHON+' '+TG2CSV)
os.system(PYTHON+' '+ANCHOR)
time_to_anchor = time.time() - start_time

infos = """Initialisation : \t"""+str(time_to_init)+""" sec
Découpage : \t"""+str(time_to_start)+""" sec
Alignement : \t"""+str(time_to_align)+""" sec
SLAM : \t"""+ str(time_to_prosodie)+""" sec
Annotations : \t"""+str(time_to_anchor)+""" sec

## TOTAL """+str(time_to_init+time_to_start+time_to_align+time_to_prosodie+time_to_anchor)+"""##\n"""

sys.stdout.write(infos)

alert_mail("TRAITEMENT FINI\n"+infos)
clean_mail()

os.system(PYTHON+' '+SAUVEGARDE)
