# -*- coding: utf-8 -*-
import tkSnack
from lxml import etree as ET
from Tkinter import *
import os
#f_path = open("./PATH.py",'r')
#path = f_path.read()
#exec(path)

tkSnack.initializeSnack(Tk())

path_to_XML = """./XML/"""
path_to_WAV = """./WAV/"""

liste_element = os.listdir(path_to_XML)
nb_FILE = 0
nb_sequence_SUP10 = 0
nb_sequence = 0
temps_total = 0
temps_seq_sup10 = 0

for element in liste_element:
	#print element
	if element.endswith('.xml') or element.endswith('.XML'):
		print "c'est un fichier xml"
		tree =  ET.parse(path_to_XML+element)
		nb_FILE+=1
		for audio in tree.xpath("/SEMORAL/TRANS/TEXT/S/AUDIO"):
			if float(audio.get('end')) - float(audio.get('start')) > 10:
				nb_sequence_SUP10+=1
				temps_seq_sup10 += float(audio.get('end')) - float(audio.get('start'))
			nb_sequence+=1
			temps_total += float(audio.get('end')) - float(audio.get('start'))
			
print "nb_FILE = "+str(nb_FILE)
print "nb_sequence = "+str(nb_sequence)
print "nb_sequence_SUP10 = "+str(nb_sequence_SUP10)
print "temps total = "+str(temps_total)
print "temps total des sequence de plus de 10 secondes = "+str(temps_seq_sup10)
