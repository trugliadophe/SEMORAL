Bienvenu dans le programme Prosodorus !
=======================================
[TOC]
PRESENTATION
------------
INSTALLATION
------------
Ce programme permet globalement de donner la prosodie de chaque mot présent dans un corpus.
Plus précisément, il a été commandé pour évaluer la prosodie des anaphores référencées par des balises <anchor> dans un corpus annoté.

Pour receuillir des résultats optimum, veuillez suivre les prérequis et étapes suivantes:

1) Téléchargez le projet en archive zip
2) Assurez-vous d'avoir les droits administrateurs
3) Lancez le programme selon les instructions suivantes

Le corpus doit être sous la forme de fichiers d'annotation intégrées (XML) et de fichiers son au format WAV

Si plusieurs fichiers XML correspondent au même fichier WAV, il est impératif de leur donner le même nom

Exemple :
fichierA.wav fichierA_1.xml fichierA_2.xml fichierA_3.xml

Les fichiers doivent être placés selon l'arboréscence suivante :

*
├── TARGET_PROSODORUS
├── IN_XML
│   ├── fichierA_1.xml
│   ├── fichierA_2.xml
│   ├── fichierA_3.xml
│   ├── fichierB.xml
│   ├── fichierC_1.xml
│   ├── fichierC_2.xml
│   ├── fichierC_3.xml
│   └── fichierC_4.xml
└── IN_WAV
    ├── fichierA.wav
    ├── fichierB.wav
    └── fichierC.wav

TARGET_PROSODORUS est le nom d'un fichier banal (qui peut être parfaitement vide) que vous devez créer à côté des dossiers IN_XML et IN_WAV contenant respectivement vos fichiers xml et wav 

* indique que vous pouvez placer ces éléments à n'importe quel endroit, le programme se charge de retrouver le fichier TARGET_PROSODORUS pour la suite des opérations

ensuite le plus simple reste à venir :
ouvrez un terminal à côté du fichier que vous êtes en train de lire et tapez : cd OPERATIONS/;./run


L'ordre des traitements est le suivant : 
1) nettoyage

options : 
-silence  permet de ne pas avoir le message d'avertissement au démarrage
--no-mail le programme ne vous propose pas l'envoi des résultats par mail
