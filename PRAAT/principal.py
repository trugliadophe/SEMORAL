# coding=utf-8
import os
import subprocess
import time
import sys
#f_path = open("../OPERATIONS/PATH.py")
f_path = open("E:\\OPERATIONS\\PATH.py",'r')
path = f_path.read()
exec(path)

a = raw_input("appuyez sur 'entrer' pour lancer le traitement des fichiers son et de transcription")

list_wav = []
list_trans = []

start_time = time.time()
path_USE_TXT = WIN_path_to_TXT if os.name == 'nt' else path_to_PRAAT_TXT
path_USE_WAV = WIN_path_to_WAV if os.name == 'nt' else path_to_PRAAT_WAV
path_USE_RESULT = WIN_path_to_RESULT if os.name == 'nt' else path_to_PRAAT_RESULT
list_element = os.listdir(path_USE_TXT)
for element in list_element:
	#print element
	if not os.path.isdir(element):
		# c'est un fichier
		(name,ext) = element.split('.')
		if ext in ['txt','TXT']:
			# c'est un fichier txt
			if os.path.exists(path_USE_WAV+name+".WAV") or os.path.exists(path_USE_WAV+name+".wav"):
				txt_file = open(path_USE_TXT+element,'r')
				content = txt_file.read()
				cpt = 0
				for s in content.split(" "):
					cpt += 1 if not s == '' else 0
				if cpt > 0:
					list_trans.append(element)
					list_wav.append(name+".wav")
					sys.stdout.write("\rlecture de "+str(len(list_trans)).zfill(len(str(len(list_element))))+"/"+str(len(list_element))+" fichiers")
					sys.stdout.flush()
sys.stdout.write("\nnombre de fichiers couple de fichiers : "+str(len(list_wav))+"\n")
a = raw_input("lecture des fichiers terminee")
list_wav.sort()
list_trans.sort()
for i in range(len(list_trans)):
	print list_trans[i]+" - "+list_wav[i]
a = raw_input("tri des fichiers termine")
for i in range(len(list_wav)):
	os.system('cls')
	line = """fichier  {}""".format(str(i+1).zfill(len(str(len(list_wav)))))+"""/{}""".format(str(len(list_wav)))
	print "#"*(len(line)+14)
	print "#"*5+"  "+line+"  "+"#"*5
	print "#"*(len(line)+14)
	(name,ext) = list_wav[i].split('.')
	subprocess.call([WIN_PRAAT,
	'--run',#lancement du script
	WIN_path_to_align_script,#emplacement du script
	path_USE_WAV + list_wav[i],#nom du fichier son
	path_USE_TXT + list_trans[i],#nom du fichier de transcription
	path_USE_RESULT + name#nom du fichier de sortie
])
file_t = open(WIN_PRAAT_time_log if os.name == 'nt' else PRAAT_time_log,'w')
file_t.write(str(time.time() - start_time))
file_t.close()
a = raw_input("traitement praat termine")
